package com.burritobuster;

import android.widget.ImageView;

public class Entity {

    // actual coordinates
    private float mX; // pixel
    private float mY; // pixel

    private boolean left; // if object is on right border

    // acceleration of axes
    private float vX; // pixel per 10ms
    private float vY; // pixel per 10ms

    // the imageview representing the entity
    private ImageView iv;

    private int yCount;
    private int jumpCount;
    private double xMultiplier;

    private boolean existsInList;


    /**
     * constructor for entity
     * @param x x-coordinate of the entity
     * @param y y-coordinate of the entity
     * @param left whether the entity spawned at the left border or not
     * @param iv ImageView representing the entity
     */
    public Entity(float x, float y, boolean left, ImageView iv) {
        this.mX = x;
        this.mY = y;

        this.vX = 10;
        this.vY= 4;
        this.xMultiplier = 1.0;

        this.left = left;

        this.iv = iv;

        this.yCount = 1;
        this.jumpCount = 45;
    }

    /**
     * constructor which is mainly used for loading from extras received from another activity
     * @param x x-coordinate of the entity
     * @param y y-corrdinate of the entity
     * @param vX x-acceleration of the entity
     * @param vY y-acceleration of the entity
     * @param yCount of the entity
     * @param jumpCount of the entity
     * @param xMultiplier of the entity
     * @param left whether the entity spawned at the left border or not
     */
    public Entity(float x, float y, float vX, float vY, int yCount, int jumpCount, double xMultiplier, boolean left) {
        this.mX = x;
        this.mY = y;

        this.vX = vX;
        this.vY = vY;

        this.left = left;

        this.yCount = yCount;
        this.jumpCount = jumpCount;

        this.xMultiplier = xMultiplier;
    }


    /**
     * update x-coordinate of the entity
     * @param speed gameSpeed influences x-coordinate
     */
    private void updateX(int speed)
    {
        if(jumpCount <= 0 && xMultiplier > 0.0)
            xMultiplier -= 0.01;


        if(left)
            this.mX += (speed + vX) * xMultiplier;

        else
            this.mX -= (speed + vX) * xMultiplier;

        iv.setX(this.mX);
    }

    /**
     * update y-coordinate of the entity
     * @param ac
     * @param speed gameSpeed influences y-coordinate
     */
    private void updateY(double ac, double speed)
    {
        double offset = 0.0;

        if(jumpCount > 0)
        {
            offset -= speed + 0.4 * (ac * jumpCount * jumpCount);
            jumpCount-=2;
        }

        else {
            offset = speed + 0.1 * (ac * yCount * yCount);
            this.yCount += 2;
        }


        this.mY += offset;
        iv.setY(this.mY);
    }


    /**
     * updates x- and y-coordinate of the entity
     * @param speed gameSpeed influences coordinates
     */
    public void update(int speed) {
        updateX(speed);
        updateY(0.03, speed);
    }

    /**
     * checks, whether this entity has a specific ImageView
     * @param iv ImageView to be checked for
     * @return this.iv == iv
     */
    public boolean hasImageView(ImageView iv) {
        return this.iv == iv;
    }

    /**
     * returns the entitys ImageView
     * @return this.iv
     */
    public ImageView getIV() { return this.iv; }

    /**
     * sets the entitys ImageView
     * @param iv ImageView to be set
     */
    public void setIV(ImageView iv) { this.iv = iv; }

    /**
     * sets the x-coordinate of the entity
     * @param x x-coordinate to be set
     */
    public void setX(float x) { this.mX = x; }

    /**
     * returns x-coordinate of the entity
     * @return x-coordinate of the entity
     */
    public float getX() { return this.mX; }

    /**
     * sets the y-coordinate of the entity
     * @param y y-coordinate to be set
     */
    public void setY(float y) { this.mY = y; }

    /**
     * returns y-coordinate of the entity
     * @return y-coordinate of the entity
     */
    public float getY() { return this.mY; }

    /**
     * returns x-acceleration of the entity
     * @return x-acceleration of the entity
     */
    public float getvX() { return this.vX; }

    /**
     * returns y-acceleration of the entity
     * @return y-acceleration of the entity
     */
    public float getvY() { return this.vX; }

    /**
     * returns yCount of the entity
     * @return yCount of the entity
     */
    public int getyCount() { return this.yCount; }

    /**
     * returns jumpCount of the entity
     * @return jumpCount of the entity
     */
    public int getJumpCount() { return this.jumpCount; }

    /**
     * returns xMultiplier of the entity
     * @return xMultiplier of the entity
     */
    public double getxMultiplier() { return this.xMultiplier; }

    /**
     * checks, whether the entity spawned at the left border or not
     * @return left border spawn or not
     */
    public boolean getLeft() { return this.left; }

}
