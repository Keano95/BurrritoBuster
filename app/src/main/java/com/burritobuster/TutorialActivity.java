package com.burritobuster;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TutorialActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
    }

    /**
     * button function to switch to MainActivity
     * @param view view clicked on
     */
    public void hideTutorial(View view) {
        // start MainActivity
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
