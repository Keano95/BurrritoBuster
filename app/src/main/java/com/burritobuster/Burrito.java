package com.burritobuster;

import android.widget.ImageView;

public class Burrito extends Entity {

    private final int mDrawable = R.drawable.burrito;

    /**
     * constructor of burrito
     * @param x x-coordinate of the burrito
     * @param y y-coordinate of the burrito
     * @param left whether the burrito spawned at the left border or not
     * @param iv ImageView of the burrito
     */
    public Burrito(float x, float y, boolean left, ImageView iv) {
        super(x,y,left,iv);
    }

    /**
     * constructor used for loading existing burritos from pause
     * @param x x-coordinate of the burrito
     * @param y y-coordinate of the burrito
     * @param vX x-acceleration of the burrito
     * @param vY y-acceleration of the burrito
     * @param yCount of the burrito
     * @param jumpCount of the burrito
     * @param xMultiplier of the burrito
     * @param left whether the burrito spawned th aht left border or not
     */
    public Burrito(float x, float y, float vX, float vY, int yCount, int jumpCount, double xMultiplier, boolean left) {
        super(x,y,vX,vY,yCount,jumpCount,xMultiplier,left);
    }

    /**
     * returns the burritos drawable ID
     * @return burrito drawable ID
     */
    public int getDrawable() { return mDrawable; }
}
