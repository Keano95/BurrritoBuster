package com.burritobuster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class HighscoreActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("HighscorePref", 0);

        // load all ten scores with players names
        ArrayList<String> list = new ArrayList<>();
        for(int i = 1; i < 11; i++) {
            String text = "";
            if(i == 1) {
                text += "1st: ";
            }
            else if(i == 2) {
                text += "2nd: ";
            }
            else if(i == 3) {
                text += "3rd: ";
            }
            else {
                text += String.valueOf(i) + "th: ";
            }
            text += pref.getString(String.valueOf(i) + "_string", "") + " scored " +
            pref.getInt(String.valueOf(i), 0) + " points";
            list.add(text);
        }

        TextView label = findViewById(R.id.label1);
        label.setText(list.get(0));
        label = findViewById(R.id.label2);
        label.setText(list.get(1));
        label = findViewById(R.id.label3);
        label.setText(list.get(2));
        label = findViewById(R.id.label4);
        label.setText(list.get(3));
        label = findViewById(R.id.label5);
        label.setText(list.get(4));
        label = findViewById(R.id.label6);
        label.setText(list.get(5));
        label = findViewById(R.id.label7);
        label.setText(list.get(6));
        label = findViewById(R.id.label8);
        label.setText(list.get(7));
        label = findViewById(R.id.label9);
        label.setText(list.get(8));
        label = findViewById(R.id.label10);
        label.setText(list.get(9));
    }

    /**
     * button function to switch to MainActivity
     * @param view
     */
    public void hideHighscore(View view) {
        // starts MainActivity
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
