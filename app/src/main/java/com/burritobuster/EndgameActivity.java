package com.burritobuster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class EndgameActivity extends Activity {

    public int score;
    public int placed;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endgame);

        Intent intent = getIntent();
        score = intent.getIntExtra(GameActivity.EXTRA_SCORE, 0);

        TextView your_score_value_label = findViewById(R.id.your_score_value_label);
        your_score_value_label.setText(String.valueOf(score));


        pref = getApplicationContext().getSharedPreferences("HighscorePref", 0);
        editor = pref.edit();

        EditText your_name_editText = findViewById(R.id.your_name_editText);
        // autofill last name entered
        your_name_editText.setText(pref.getString("lastName", "Enter Name"));
        updateHighscore();

    }

    /**
     * add and order new score in existing top ten score list
     * @param place place achieved by the player
     * @param score score achieved by the player
     */
    private void sortHighscore(int place, int score) {
        this.placed = place;

        if(place == 10) {
            editor.putInt("10", score);
        }

        for(int i = 10; i > place; i--) {
            // sink every score till placed score
            editor.putInt(String.valueOf(i), pref.getInt(String.valueOf(i - 1), 0));
            // also sink the according names
            editor.putString(String.valueOf(i) + "_string", pref.getString(String.valueOf(i - 1) + "_string", ""));
        }
        // update placed place score
        editor.putInt(String.valueOf(place), score);

        editor.apply();
        // name of placed place will be updated when leaving EndgameActivity!!!
    }

    /**
     * updates string for the place entered by the player
     */
    private void updatePlacedText() {
        EditText your_name_editText = findViewById(R.id.your_name_editText);
        String name = your_name_editText.getText().toString();
        editor.putString(String.valueOf(placed) + "_string", name);
        editor.putString("lastName", name);
        editor.apply();
    }

    /**
     * button function to switch to MainActivity
     * saves entered name by the player
     * @param view view clicked on
     */
    public void endGame_toHome(View view) {
        updatePlacedText();

        // start MainActivity
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * button function to restart the game in GameActivity
     * saves entered name by the player
     * @param view view clicked on
     */
    public void endGame_nochmal(View view) {
        updatePlacedText();

        // start GameActivity
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * check which place is achieved by the player
     * and update the top ten score list if needed
     */
    private void updateHighscore() {
        // get old Highscore, maybe update it and show it
        int nowHighscore = pref.getInt("1", 0);
        if(score > nowHighscore) {
            nowHighscore = score;
        }
        TextView highscore_ende_value_label = findViewById(R.id.highscore_ende_value_label);
        highscore_ende_value_label.setText(String.valueOf(nowHighscore));

        // get position of players score in top ten
        int placeScore = 0;
        int temp = 0;
        for(int i = 10; i >= 0; i--) {
            placeScore = pref.getInt(String.valueOf(i), 0);
            // if lower score is found, leave for and update top ten list
            temp = i;
            if(score < placeScore) {
                break;
            }
        }

        TextView you_placed_value_label = findViewById(R.id.you_placed_value_label);
        switch(temp) {
            case 10: // 11th
                you_placed_value_label.setText(getResources().getString(R.string._11th));
                sortHighscore(11, score);
                break;
            case 9: // 10th
                you_placed_value_label.setText(getResources().getString(R.string._10th));
                sortHighscore(10, score);
                break;
            case 8: // 9th
                you_placed_value_label.setText(getResources().getString(R.string._9th));
                sortHighscore(9, score);
                break;
            case 7: // 8th
                you_placed_value_label.setText(getResources().getString(R.string._8th));
                sortHighscore(8, score);
                break;
            case 6: // 7th
                you_placed_value_label.setText(getResources().getString(R.string._7th));
                sortHighscore(7, score);
                break;
            case 5: // 6th
                you_placed_value_label.setText(getResources().getString(R.string._6th));
                sortHighscore(6, score);
                break;
            case 4: // 5th
                you_placed_value_label.setText(getResources().getString(R.string._5th));
                sortHighscore(5, score);
                break;
            case 3: // 4th
                you_placed_value_label.setText(getResources().getString(R.string._4th));
                sortHighscore(4, score);
                break;
            case 2: // 3rd
                you_placed_value_label.setText(getResources().getString(R.string._3rd));
                sortHighscore(3, score);
                break;
            case 1: // 2nd
                you_placed_value_label.setText(getResources().getString(R.string._2nd));
                sortHighscore(2, score);
                break;
            case 0: // 1st
                you_placed_value_label.setText(getResources().getString(R.string._1st));
                sortHighscore(1, score);
                break;
            default:
                break;
        }
    }

}
