package com.burritobuster;

import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import static com.burritobuster.GameActivity.EXTRA_SPAWNCOUNTER;
import static com.burritobuster.GameActivity.STATE_LIFE;
import static com.burritobuster.GameActivity.STATE_SCORE;

public class PauseActivity extends Activity {

    // data received from GameActivity
    private ArrayList<BurritoParcelable> burrito_parcelables;
    private ArrayList<SombreroParcelable> sombrero_parcelables;
    private int number_lifes;
    private int score;
    private int spawnCounter;
    private int gameSpeed;
    private int gameSpeedCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause);

        // read all Extras
        // if EXTRA_SCORE is there, the other should be as well
        if(getIntent().hasExtra(GameActivity.EXTRA_SCORE)) {
            Intent intent = getIntent();
            score = intent.getIntExtra(GameActivity.EXTRA_SCORE, 0);
            number_lifes = intent.getIntExtra(GameActivity.EXTRA_LIFES, 3);
            spawnCounter = intent.getIntExtra(GameActivity.EXTRA_SPAWNCOUNTER, 0);
            gameSpeed = intent.getIntExtra(GameActivity.EXTRA_GAMESPEED, 0);
            gameSpeedCounter = intent.getIntExtra(GameActivity.EXTRA_GAMESPEEDCOUNTER, 0);
            burrito_parcelables = intent.getParcelableArrayListExtra(GameActivity.EXTRA_BURRITOS);
            sombrero_parcelables = intent.getParcelableArrayListExtra(GameActivity.EXTRA_SOMBREROS);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt(STATE_SCORE, score);
        savedInstanceState.putInt(STATE_LIFE, number_lifes);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * button function to switch to MainActivity
     * all ingame data will be lost
     * no highscore update
     * @param view view clicked on
     */
    public void pause_toHome(View view) {
        // starts MainActivity -> no need to keep track of data
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * button function to continue the game in GameActivity
     * all ingame data will be send to the other Activity
     * @param view view clicked on
     */
    public void pause_weiter(View view) {
        // start GameActivity with extra data to resume the game
        Intent intent = new Intent(this, GameActivity.class);

        intent.putExtra(GameActivity.EXTRA_SCORE, score);
        intent.putExtra(GameActivity.EXTRA_LIFES, number_lifes);
        intent.putExtra(GameActivity.EXTRA_SPAWNCOUNTER, spawnCounter);
        intent.putExtra(GameActivity.EXTRA_GAMESPEED, gameSpeed);
        intent.putExtra(GameActivity.EXTRA_GAMESPEEDCOUNTER, gameSpeedCounter);

        intent.putParcelableArrayListExtra(GameActivity.EXTRA_BURRITOS, burrito_parcelables);
        intent.putParcelableArrayListExtra(GameActivity.EXTRA_SOMBREROS, sombrero_parcelables);
        startActivity(intent);
    }

    /**
     * button function to restart the game in GameActivity
     * all ingame data will be lost
     * no highscore update
     * @param view view clicked on
     */
    public void pause_neustart(View view) {
        // start GameActivity without extra data so game restarts
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

}
