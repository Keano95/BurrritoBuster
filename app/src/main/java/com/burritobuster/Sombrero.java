package com.burritobuster;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class Sombrero extends Entity {

    private final int mDrawable = R.drawable.sombrero;

    /**
     * constructor of sombrero
     * @param x x-coordinate of the sombrero
     * @param y y-coordinate of the sombrero
     * @param left whether the sombrero spawned at the left border or not
     * @param iv ImageView of the sombrero
     */
    public Sombrero(float x, float y, boolean left, ImageView iv) {
        super(x, y, left, iv);
    }

    /**
     * constructor used for loading existing sombreros from pause
     * @param x x-coordinate of the sombrero
     * @param y y-coordinate of the sombrero
     * @param vX x-acceleration of the sombrero
     * @param vY y-acceleration of the sombrero
     * @param yCount of the sombrero
     * @param jumpCount of the sombrero
     * @param xMultiplier of the sombrero
     * @param left whether the sombrero spawned th aht left border or not
     */
    public Sombrero(float x, float y, float vX, float vY, int yCount, int jumpCount, double xMultiplier, boolean left) {
        super(x, y, vX, vY, yCount, jumpCount, xMultiplier, left);
    }

    /**
     * returns the sombreros drawable ID
     * @return sombrero drawable ID
     */
    public int getDrawable() {
        return mDrawable;
    }
}
