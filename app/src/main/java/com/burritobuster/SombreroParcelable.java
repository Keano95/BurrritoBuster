package com.burritobuster;

import android.os.Parcel;
import android.os.Parcelable;

public class SombreroParcelable implements Parcelable{

    // actual coordinates
    private float mX; // pixel
    private float mY; // pixel
    private boolean left; // if object is on right border
    // acceleration of axes
    private float vX; // pixel per 10ms
    private float vY; // pixel per 10ms
    private int yCount;
    private int jumpCount;
    private double xMultiplier;
    @Override
    public int describeContents() {
        return 0; }@Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(mX);
        parcel.writeFloat(mY);
        parcel.writeFloat(vX);
        parcel.writeFloat(vY);
        parcel.writeByte((byte) (left ? 1 : 0));
        parcel.writeInt(yCount);
        parcel.writeInt(jumpCount);
        parcel.writeDouble(xMultiplier); }public static final Parcelable.Creator<com.burritobuster.SombreroParcelable> CREATOR
            = new Parcelable.Creator<com.burritobuster.SombreroParcelable>() {
        public com.burritobuster.SombreroParcelable createFromParcel(Parcel in) {
            return new com.burritobuster.SombreroParcelable(in); }public com.burritobuster.SombreroParcelable[] newArray(int size) {
            return new com.burritobuster.SombreroParcelable[size]; }};
    /**
     * constructor reads all information from existing parcel
     * @param in parcel to read data from
     */
    private SombreroParcelable(Parcel in) {
        this.mX = in.readFloat();
        this.mY = in.readFloat();
        this.vX = in.readFloat();
        this.vY = in.readFloat();
        this.left = in.readByte() != 0;
        this.yCount = in.readInt();
        this.jumpCount = in.readInt();
        this.xMultiplier = in.readDouble();
    }

    /**
     * constructor, used to load data from sombrero
     * @param x x-coordinate of the sombrero
     * @param y y-coordinate of the sombrero
     * @param vX x-acceleration of the sombrero
     * @param vY y-acceleration of the sombrero
     * @param yCount of the sombrero
     * @param jumpCount of the sombrero
     * @param xMultiplier of the sombrero
     * @param left whether the sombrero spawned at the left border or not
     */
    public SombreroParcelable(float x, float y, float vX, float vY, int yCount, int jumpCount, double xMultiplier, boolean left) {
        this.mX = x;
        this.mY = y;
        this.vX = vX;
        this.vY = vY;
        this.left = left;
        this.yCount = yCount;
        this.jumpCount = jumpCount;
        this.xMultiplier = xMultiplier;
    }

    /**
     * returns x-coordinate of the sombrero
     * @return x-coordinate of the sombrero
     */
    public float getX() {
        return this.mX;
    }

    /**
     * returns y-coordinate of the sombrero
     * @return y-coordinate of the sombrero
     */
    public float getY() {
        return this.mY;
    }

    /**
     * returns x-acceleration of the sombrero
     * @return x-acceleration of the sombrero
     */
    public float getvX() {
        return this.vX;
    }

    /**
     * returns y-acceleration of the sombrero
     * @return y-acceleration of the sombrero
     */
    public float getvY() {
        return this.vY;
    }

    /**
     * returns yCount of the sombrero
     * @return yCount of the sombrero
     */
    public int getyCount() {
        return this.yCount;
    }

    /**
     * returns jumpCount of the sombrero
     * @return jumpCount of the sombrero
     */
    public int getJumpCount() {
        return this.jumpCount;
    }

    /**
     * returns xMultiplier of the sombrero
     * @return xMultiplier of the sombrero
     */
    public double getxMultiplier() {
        return this.xMultiplier;
    }

    /**
     * checks, whether sombrero spawned at the left border or not
     * @return left border spawn or not
     */
    public boolean getLeft() {
        return this.left;
    }
}

