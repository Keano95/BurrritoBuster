package com.burritobuster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class GameActivity extends Activity {

    public RelativeLayout game_view;
    private TextView new_highscore_label;
    public static int screen_width;
    public static int screen_height;
    public int burrito_height_width;
    public int sombrero_height_width;

    public int game_view_height;
    public int game_view_width;

    // strings where the extras are stored when send to another activity
    public static final String EXTRA_BURRITOS = "com.burritobuster.BURRITOS";
    public static final String EXTRA_SOMBREROS = "com.burritobuster.SOMBREROS";
    public static final String EXTRA_LIFES = "com.burritobuster.LIFES";
    public static final String EXTRA_SCORE = "com.burritobuster.SCORE";
    public static final String EXTRA_SPAWNCOUNTER = "com.burritobuster.SPAWNCOUNTER";
    public static final String EXTRA_GAMESPEED = "com.burritobuster.GAMESPEED";
    public static final String EXTRA_GAMESPEEDCOUNTER = "com.burritobuster.GAMESPEEDCOUNTER";

    // strings where data is stored in saveInstanceBundle
    public static final String STATE_BURRITOS = "playersBurritos";
    public static final String STATE_SCORE = "playersScore";
    public static final String STATE_LIFE = "playersLifes";
    public static final String STATE_SPAWNCOUNTER = "playersSpawnCounter";
    public static final String STATE_GAMESPEED = "playersGameSpeed";
    public static final String STATE_GAMESPEEDCOUNTER = "playersGameSpeedCounter";

    // ArrayLists for entities and parcelables
    public ArrayList<Burrito> mBurritos;
    public ArrayList<Sombrero> mSombreros;
    public ArrayList<BurritoParcelable> mBurritoParcelable;
    public ArrayList<SombreroParcelable> mSombreroParcelable;

    // handler and runnable for moving spawning objects
    private Handler gameHandler;
    private Runnable gameRunnable;

    //Game misc
    public int number_lifes;
    public int score;
    private int spawnCounter;
    private int gameSpeed;
    private int gameSpeedCounter;

    private boolean loaded;

    //TODO: if intent has Extra -> resume game, else start new game
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        game_view = findViewById(R.id.game_view);
        game_view_height = game_view.getHeight();
        game_view_width = game_view.getWidth();
        game_view.setBackground(getDrawable(R.drawable.mexicobackground));

        mBurritos = new ArrayList<>();
        mSombreros = new ArrayList<>();
        mBurritoParcelable = new ArrayList<>();
        mSombreroParcelable = new ArrayList<>();

        burrito_height_width = (int) getResources().getDimension(R.dimen.burrito_height);
        sombrero_height_width = (int) getResources().getDimension(R.dimen.burrito_height);

        // get display size, important to set Objects at phones borders
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screen_width = size.x;
        screen_height = size.y;

        new_highscore_label = findViewById(R.id.new_highscore_label);
    }

    // ON... FUNCTIONS------------------------------------------------------------------------------
    // TODO: do we need those on.. functions? maybe for saving game state?
    @Override
    public void onResume() {
        super.onResume();

        loadExtras();

        runGame();
    }

    @Override
    public void onPause() {
        super.onPause();

        Intent intent = getIntent();
        sendExtras(intent);
    }

    @Override
    public void onStop() {
        super.onStop();

        Intent intent = getIntent();
        sendExtras(intent);
    }

    @Override
    public void onRestart() {
        super.onRestart();

        loadExtras();

        runGame();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt(STATE_SCORE, score);
        savedInstanceState.putInt(STATE_LIFE, number_lifes);
        savedInstanceState.putInt(STATE_SPAWNCOUNTER, spawnCounter);
        savedInstanceState.putInt(STATE_GAMESPEED, gameSpeed);
        savedInstanceState.putInt(STATE_GAMESPEEDCOUNTER, gameSpeedCounter);

        burritoToParcelable();
        savedInstanceState.putParcelableArrayList(STATE_BURRITOS, mBurritoParcelable);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            score = savedInstanceState.getInt(STATE_SCORE);
            updateScore();
            number_lifes = savedInstanceState.getInt(STATE_LIFE);
            updateLifes();
            spawnCounter = savedInstanceState.getInt(STATE_SPAWNCOUNTER);
            gameSpeed = savedInstanceState.getInt(STATE_GAMESPEED);
            gameSpeedCounter = savedInstanceState.getInt(STATE_GAMESPEEDCOUNTER);
            mBurritoParcelable = savedInstanceState.getParcelableArrayList(STATE_BURRITOS);

            parcelableToBurrito();
        }
    }

    // GAME THREAD:---------------------------------------------------------------------------------

    /**
     * game thread, uses handler to update UI every 10ms
     */
    private void runGame() {
        this.gameSpeed = 1;
        this.gameSpeedCounter = 0;


        gameHandler = new Handler();
        gameRunnable = new Runnable() {


            @Override
            public void run() {
                update();
                gameHandler.postDelayed(this, 10);
            }
        };
        gameHandler.post(gameRunnable);
    }

    /**
     * updates game, including spawns, acceleration and movement of burritos and sombreros
     */
    private void update()
    {
        //spawn burrito
        if(spawnCounter >= (1000 - (150 * this.gameSpeed)))
        {
            Random r = new Random();

            if(r.nextInt(100) < 70)
                spawnEnitiy(R.drawable.burrito);
            else
                spawnEnitiy(R.drawable.sombrero);

            spawnCounter = 0;
        }

        //accelerate game
        if(gameSpeedCounter >= 10000)
        {
            this.gameSpeed++;
            gameSpeedCounter = 0;
        }

            Iterator<Burrito> iterator = mBurritos.iterator();
            while(iterator.hasNext()) {
                Burrito burrito = iterator.next();
                burrito.update(this.gameSpeed);

            if (burrito.getY() > screen_height) { // if burrito left window on the bottom border
                iterator.remove();
                removeLife(burrito.getIV());
            }
            else if (burrito.getLeft() && burrito.getX() > screen_width) { // if burrito left window on the right border
                iterator.remove();
                removeLife(burrito.getIV());
            }
            else if (!burrito.getLeft() && burrito.getX() < -burrito_height_width) { // if burrito left window in the left border
                iterator.remove();
                removeLife(burrito.getIV());
            }
        }


        Iterator<Sombrero> it = mSombreros.iterator();
        while(it.hasNext()) {
            Sombrero sombrero = it.next();
            sombrero.update(this.gameSpeed);

            if (sombrero.getY() > screen_height)  // if burrito left window on the bottom border
                it.remove();

            else if (sombrero.getLeft() && sombrero.getX() > screen_width)  // if burrito left window on the right border
                it.remove();

            else if (!sombrero.getLeft() && sombrero.getX() < -sombrero_height_width)  // if burrito left window in the left border
                it.remove();

        }

        spawnCounter += 10;
        gameSpeedCounter += 10;
    }

    // HELPER FUNCTIONS:----------------------------------------------------------------------------

    // EXTRA FUNCTIONS:-----------------------------------------------------------------------------

    /**
     * all burritos and sombreros to parcelable,to be able to be transferred to another activity
     */
    private void burritoToParcelable() {
        mBurritoParcelable.clear();
        mBurritos.forEach((elem) -> {
            mBurritoParcelable.add(new BurritoParcelable(elem.getX(), elem.getY(),
                    elem.getvX(), elem.getvY(), elem.getyCount(), elem.getJumpCount(),
                    elem.getxMultiplier(), elem.getLeft()));
        });
        mBurritos.clear();

        mSombreroParcelable.clear();
        mSombreros.forEach((elem) -> {
            mSombreroParcelable.add(new SombreroParcelable(elem.getX(), elem.getY(),
                    elem.getvX(), elem.getvY(), elem.getyCount(), elem.getJumpCount(),
                    elem.getxMultiplier(), elem.getLeft()));
        });
        mSombreros.clear();
    }

    /**
     * all parcelables received from another activity, to burritos and sombreros
     * adding loaded entities to UI
     */
    private void parcelableToBurrito() {
        // turn mBurritoParcelable in burritos
        mBurritos.clear();
        mBurritoParcelable.forEach((elem) -> {
            mBurritos.add(new Burrito(elem.getX(), elem.getY(),
                    elem.getvX(), elem.getvY(), elem.getyCount(), elem.getJumpCount(),
                    elem.getxMultiplier(), elem.getLeft()));
        });
        mBurritoParcelable.clear();

        // get all burritos on screen
        mBurritos.forEach((elem) -> {
            updateEntity(elem);
            createEntity(elem.getX(), elem.getY(), elem.getLeft(), elem.getDrawable(), elem.getIV());
        });


        // turn mSombreroParcelable in sombreros
        mSombreros.clear();
        mSombreroParcelable.forEach((elem) -> {
            mSombreros.add(new Sombrero(elem.getX(), elem.getY(),
                    elem.getvX(), elem.getvY(), elem.getyCount(), elem.getJumpCount(),
                    elem.getxMultiplier(), elem.getLeft()));
        });
        mSombreroParcelable.clear();

        // get all burritos on screen
        mSombreros.forEach((elem) -> {
            updateEntity(elem);
            createEntity(elem.getX(), elem.getY(), elem.getLeft(), elem.getDrawable(), elem.getIV());
        });
    }

    /**
     * load all extras send by another activity
     * if extras loaded, update life and score
     */
    private void loadExtras() {
        loaded = false;
        if(getIntent().hasExtra(EXTRA_SCORE)) {
            loaded = true;
            Intent intent = getIntent();
            score = intent.getIntExtra(EXTRA_SCORE, 0);
            updateScore();
            number_lifes = intent.getIntExtra(EXTRA_LIFES, 3);
            updateLifes();
            spawnCounter = intent.getIntExtra(EXTRA_SPAWNCOUNTER, 0);
            gameSpeed = intent.getIntExtra(EXTRA_GAMESPEED, 0);
            gameSpeedCounter = intent.getIntExtra(EXTRA_GAMESPEEDCOUNTER, 0);
            mBurritoParcelable = intent.getParcelableArrayListExtra(EXTRA_BURRITOS);
            mSombreroParcelable = intent.getParcelableArrayListExtra(EXTRA_SOMBREROS);

            parcelableToBurrito();
        }
        if(!loaded) {
            score = 0;
            updateScore();
            number_lifes = 3;
            updateLifes();
        }
    }

    /**
     * add extras to be send to a specific intent
     * @param intent is prepared for activity switch
     */
    private void sendExtras(Intent intent) {
        intent.putExtra(EXTRA_SCORE, score);
        intent.putExtra(EXTRA_LIFES, number_lifes);
        intent.putExtra(EXTRA_SPAWNCOUNTER, spawnCounter);
        intent.putExtra(EXTRA_GAMESPEED, gameSpeed);
        intent.putExtra(EXTRA_GAMESPEEDCOUNTER, gameSpeedCounter);

        burritoToParcelable();

        intent.putParcelableArrayListExtra(EXTRA_BURRITOS, mBurritoParcelable);
        intent.putParcelableArrayListExtra(EXTRA_SOMBREROS, mSombreroParcelable);
    }

    // SPAWN FUNCTIONS:-----------------------------------------------------------------------------

    /**
     * spawns entity at random position at left or right border
     * @param drawable burrito or sombrero drawable
     */
    private void spawnEnitiy(int drawable) {
        int half_height = screen_height / 2;
        Random rng = new Random();

        // only spawn on left or right border
        int randomX = rng.nextInt(100);
        boolean left = true;
        if(randomX >= 50) {
            randomX = screen_width;
            left = false;
        }
        else {
            // spawning burrito not visible until its moving
            if(drawable == R.drawable.burrito)
                randomX = -burrito_height_width;

            else
                randomX = -sombrero_height_width;
        }
        // only spawn in lower half of device
        int randomY = rng.nextInt(half_height)+(half_height/2);
        createEntity(randomX, randomY, left, drawable, null);
    }

    /**
     * add onClick to ImageView, add ImageView to game_view, set coordinates of ImageView
     * @param x x-Coordinate of ImageView
     * @param y y-Coordinate of ImageView
     * @param left if the entity started at the left border or not
     * @param drawable which entity has to be added to game_view and list
     * @param iv if the entity already hasan ImageView
     */
    private void createEntity(float x, float y, boolean left, int drawable, ImageView iv) {
        boolean ivset = true; // elem got loaded from pause
        if(iv == null) {
            ivset = false; // elem is new
            if(drawable == R.drawable.burrito)
                iv = createImageView(drawable,burrito_height_width,burrito_height_width);

            else if(drawable == R.drawable.sombrero)
                iv = createImageView(drawable,sombrero_height_width,sombrero_height_width);

        }

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catchObject(v,drawable==R.drawable.burrito);
            }
        });

        iv.setX(x);
        iv.setY(y);

        if (drawable == R.drawable.burrito) {
            game_view.addView(iv, burrito_height_width, burrito_height_width);
            if(!ivset) { // burrito already in list
                mBurritos.add(new Burrito(x, y, left, iv));
            }
        } else if (drawable == R.drawable.sombrero) {
            game_view.addView(iv, sombrero_height_width, sombrero_height_width);
            if(!ivset) { // sombrero already in list
                mSombreros.add(new Sombrero(x, y, left, iv));
            }
        }
    }

    /**
     * adds an ImageView to the entity
     * @param e entity to be updated
     */
    private void updateEntity(Entity e)
    {
        ImageView view = null;

        if(e.getClass() == Burrito.class){
             view = createImageView(R.drawable.burrito, burrito_height_width, burrito_height_width);
        }

        else if(e.getClass() == Sombrero.class) {
            view = createImageView(R.drawable.sombrero, burrito_height_width, burrito_height_width);
        }

        if(view == null)
            return;

        e.setIV(view);
    }

    /**
     * create an ImageView for burrito or sombrero with specific settings
     * @param drawable which drawable has to be added to ImageView
     * @param width width of the ImageView
     * @param height height of the ImageViwe
     * @return created ImageView for entity
     */
    private ImageView createImageView(int drawable, int width, int height)
    {
         final ImageView iv = new ImageView(getApplicationContext());
         iv.setImageDrawable(getDrawable(drawable));
         ViewGroup.LayoutParams layout_params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
         iv.setLayoutParams(layout_params);
         iv.getLayoutParams().height = height;
         iv.getLayoutParams().width = width;
         iv.setScaleType(ImageView.ScaleType.FIT_XY);
         iv.setVisibility(View.VISIBLE);
         iv.requestLayout();
         iv.setClickable(true);

         return iv;
    }

    // UPDATE FUNCTIONS:----------------------------------------------------------------------------

    /**
     * onClick function for sombrero and burrito
     * burrito increases score
     * sombrero decreases number_lifes
     * @param view view which was clicked on
     * @param burrito onClick depends on whether its an burrito or not
     */
    public void catchObject(View view, boolean burrito) {
        // update score
        if(burrito) {
            score += 1;
            updateScore();
            removeBurrito((ImageView) view);
        }

        else {
            number_lifes -= 1;
            updateLifes();
            removeSombrero((ImageView) view);
        }
    }

    /**
     * updates score and score_label
     * maybe shows new_highscore_label
     */
    private void updateScore() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("HighscorePref", 0);
        if(score >= pref.getInt("1", 0)) {
            new_highscore_label.setVisibility(View.VISIBLE);
        }
        TextView score_label = findViewById(R.id.score_label);
        score_label.setText(String.valueOf(score));
    }

    /**
     * update life labels
     * maybe end the game if number_lifes == 0
     */
    private void updateLifes() {
        TextView first_life_label = findViewById(R.id.first_life_label);
        TextView second_life_label = findViewById(R.id.second_life_label);
        TextView third_life_label = findViewById(R.id.third_life_label);

        // only need to update all red ones, because GameActivity starts with all green
        switch (number_lifes) {
            case 2:
                first_life_label.setBackgroundColor(0xFFCC0000);
                break;
            case 1:
                first_life_label.setBackgroundColor(0xFFCC0000);
                second_life_label.setBackgroundColor(0xFFCC0000);
                break;
            case 0:
                first_life_label.setBackgroundColor(0xFFCC0000);
                second_life_label.setBackgroundColor(0xFFCC0000);
                third_life_label.setBackgroundColor(0xFFCC0000);
                endGame();
            default:
                break;
        }
    }

    /**
     * stop game, send score to EndgameActivity, switch to EndgameActivity
     */
    private void endGame() {
        gameHandler.removeCallbacks(gameRunnable);

        // start EndGameActivity
        Intent intent = new Intent(this, EndgameActivity.class);
        intent.putExtra(EXTRA_SCORE, score);
        startActivity(intent);
    }

    // REMOVE FUNCTIONS:----------------------------------------------------------------------------

    /**
     * update lifes UI and data
     * remove out of bounds ImageView
     * @param iv ImageView to be removed from game_view
     */
    private void removeLife(ImageView iv) {
        number_lifes -= 1;
        updateLifes();
        game_view.removeView(iv);
    }

    /**
     * remove burrito from UI and burritos list
     * @param iv ImageView to be removed from game_view
     */
    private void removeBurrito(ImageView iv) {
        // delete / remove that specific burrito from every list and view
        game_view.removeView(iv);

        Iterator<Burrito> iterator = mBurritos.iterator();
        while(iterator.hasNext()) {
            Burrito burrito = iterator.next();
            if (burrito.hasImageView(iv)) {
                iterator.remove();
            }
        }
    }

    /**
     * remove sombrero from UI and sombreros list
     * @param iv ImageView to be removed from game_view
     */
    private void removeSombrero(ImageView iv) {
        // delete / remove that specific burrito from every list and view
        game_view.removeView(iv);

        Iterator<Sombrero> iterator = mSombreros.iterator();
        while(iterator.hasNext()) {
            Sombrero sombrero = iterator.next();
            if (sombrero.hasImageView(iv)) {
                iterator.remove();
            }
        }
    }

    // BUTTON FUNCTIONS:----------------------------------------------------------------------------

    /**
     * pause the game
     * send all information to PauseActivity, switch to PauseActivity
     * @param view view clicked on
     */
    public void ingame_pause(View view) {
        gameHandler.removeCallbacks(gameRunnable);

        Intent intent = new Intent(this, PauseActivity.class);

        sendExtras(intent);

        startActivity(intent);
    }

}


