package com.burritobuster;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

public class BurritoParcelable implements Parcelable{
    // actual coordinates
    private float mX; // pixel
    private float mY; // pixel

    private boolean left; // if object is on right border

    // acceleration of axes
    private float vX; // pixel per 10ms
    private float vY; // pixel per 10ms

    private int yCount;
    private int jumpCount;
    private double xMultiplier;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(mX);
        parcel.writeFloat(mY);
        parcel.writeFloat(vX);
        parcel.writeFloat(vY);
        parcel.writeByte((byte) (left ? 1 : 0));
        parcel.writeInt(yCount);
        parcel.writeInt(jumpCount);
        parcel.writeDouble(xMultiplier);
    }

    public static final Parcelable.Creator<BurritoParcelable> CREATOR
            = new Parcelable.Creator<BurritoParcelable>() {
        public BurritoParcelable createFromParcel(Parcel in) {
            return new BurritoParcelable(in);
        }

        public BurritoParcelable[] newArray(int size) {
            return new BurritoParcelable[size];
        }
    };

    /**
     * constructor reads all information from existing parcel
     * @param in parcel to read data from
     */
    private BurritoParcelable(Parcel in) {
        this.mX = in.readFloat();
        this.mY = in.readFloat();
        this.vX = in.readFloat();
        this.vY = in.readFloat();
        this.left = in.readByte() != 0;
        this.yCount = in.readInt();
        this.jumpCount = in.readInt();
        this.xMultiplier = in.readDouble();
    }

    /**
     * constructor, used to load data from burrito
     * @param x x-coordinate of the burrito
     * @param y y-coordinate of the burrito
     * @param vX x-acceleration of the burrito
     * @param vY y-acceleration of the burrito
     * @param yCount of the burrito
     * @param jumpCount of the burrito
     * @param xMultiplier of the burrito
     * @param left whether the burrito spawned at the left border or not
     */
    public BurritoParcelable(float x, float y, float vX, float vY, int yCount, int jumpCount, double xMultiplier, boolean left) {
        this.mX = x;
        this.mY = y;

        this.vX = vX;
        this.vY = vY;

        this.left = left;

        this.yCount = yCount;
        this.jumpCount = jumpCount;

        this.xMultiplier = xMultiplier;
    }

    /**
     * returns x-coordinate of the burrito
     * @return x-coordinate of the burrito
     */
    public float getX() {
        return this.mX;
    }

    /**
     * returns y-coordinate of the burrito
     * @return y-coordinate of the burrito
     */
    public float getY() {
        return this.mY;
    }

    /**
     * returns x-acceleration of the burrito
     * @return x-acceleration of the burrito
     */
    public float getvX() {
        return this.vX;
    }

    /**
     * returns y-acceleration of the burrito
     * @return y-acceleration of the burrito
     */
    public float getvY() {
        return this.vY;
    }

    /**
     * returns yCount of the burrito
     * @return yCount of the burrito
     */
    public int getyCount() {
        return this.yCount;
    }

    /**
     * returns jumpCount of the burrito
     * @return jumpCount of the burrito
     */
    public int getJumpCount() {
        return this.jumpCount;
    }

    /**
     * returns xMultiplier of the burrito
     * @return xMultiplier of the burrito
     */
    public double getxMultiplier() {
        return this.xMultiplier;
    }

    /**
     * checks, whether burrito spawned at the left border or not
     * @return left border spawn or not
     */
    public boolean getLeft() {
        return this.left;
    }

}
