package com.burritobuster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getApplicationContext().getSharedPreferences("HighscorePref", 0);
        editor = pref.edit();

        // if no highscore exists, create default top ten with all 0 as score
        boolean existingHighscore = pref.getBoolean("existingHighscore", false);
        if(!existingHighscore) {
            editor.putBoolean("existingHighscore", true);

            // store default names
            editor.putString("1_string", "Erster");
            editor.putString("2_string", "Zweiter");
            editor.putString("3_string", "Dritter");
            editor.putString("4_string", "Vierter");
            editor.putString("5_string", "Fünfter");
            editor.putString("6_string", "Sechster");
            editor.putString("7_string", "Siebter");
            editor.putString("8_string", "Achter");
            editor.putString("9_string", "Neunter");
            editor.putString("10_string", "Zehnter");

            // store default scores
            editor.putInt("1", 0);
            editor.putInt("2", 0);
            editor.putInt("3", 0);
            editor.putInt("4", 0);
            editor.putInt("5", 0);
            editor.putInt("6", 0);
            editor.putInt("7", 0);
            editor.putInt("8", 0);
            editor.putInt("9", 0);
            editor.putInt("10", 0);
        }
    }

    /**
     * button function to start game in GameActivity
     * @param view view clicked on
     */
    public void startGame(View view) {
        // start GameActivity -> automatically runs game
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * button function to switch to TutorialActivity
     * @param view view clicked on
     */
    public void showTutorial(View view) {
        // start TutorialActivity
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);

    }

    /**
     * button function to switch to HighscoreActivity
     * @param view view clicked on
     */
    public void showHighscore(View view) {
        // start HighscoreActivity
        Intent intent = new Intent(this, HighscoreActivity.class);
        startActivity(intent);
    }


}
